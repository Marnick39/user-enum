#! /usr/bin/python

import socket
import sys
import time

# ----- Config -----
# Wordlist file 
usernameFilename = "usernames.txt"

targetIP = ""
targetPort = 25

# Seconds between VRFY requests
enumSleep = 0
# Seconds between connection retries
reconnectSleep = 2
# ------------------

def reconnect(s):
  s.close()
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  connect = s.connect((targetIP,targetPort))
  print("##### - Reconnecting to " + targetIP + " on port " + str(targetPort) + "!")
  # Try to fetch banner..
  try:
    banners = s.recv(1024)
  except:
    pass
  return s
  
# Read usernames
usernameFile = open(usernameFilename, "r")
usernames = usernameFile.readlines()
totalAmountofUsernames = len(usernames)


# Create progression-tracking & statistical variables
usernameIndex = 0
userAmountDigitCount = len(str(totalAmountofUsernames))
startTime = time.time()
restartCount = 0

# Booty storage
validUsernames = []

# Connect to mailserver
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.settimeout(10)
print("Connecting..")
connect = s.connect((targetIP,targetPort))
print("Connected to " + targetIP + " on port " + str(targetPort) + "!")
print("Fetching banner..")
# Try to fetch banner..
try:
  banner = s.recv(1024).decode()
  print(banner)
except:
  print("No banner..")

print("######################################################")
# Enumerate users
for username in usernames: 
  usernameIndex += 1
  username = username.strip("\n")
  
  # Send user check
  testString = "VRFY " + username + "\r\n"
  try:
    s.send(testString.encode())
    result = s.recv(1024).decode()
    userbackup = username
  except:
    usernameIndex -= 1
    print("Something went wrong, will retry this user at the end.")
    # Append the username on which the exception triggered to the usernames list (brace infinite loop :)
    usernames.append(userbackup)
    time.sleep(reconnectSleep)
    s = reconnect(s)
    restartCount += 1
    
    # Weird exception thing makes the for loop think it's in the future.. do the next one manually:
    s.send(testString.encode())
    result = s.recv(1024).decode()
  result = result.strip("\n").strip("\r")
  
  # Create progress indicator
  progressIndicator = "|" + str(usernameIndex).rjust(userAmountDigitCount, ' ') + "/" + str(totalAmountofUsernames)
  
  # Adjust this to your needs, responses can vary depending on the mailserver
  if result.find("User unknown") == -1 and result.find("Argument required") == -1 and result.find("too many errors") == -1:
    print("[++" + progressIndicator + "| Correct username: " + username) 
    validUsernames.append(username)
  else:
    print("[--" + progressIndicator + "| Incorrect username: " + username) 
  print("       |    > " + result)
  time.sleep(enumSleep)

s.close()

# Process usernames
uniqueUsernames = set()
for uname in validUsernames:
  pos = uname.find("@")
  if pos != -1:
    uname = uname[0:pos]
  uname = uname.lower()
  uniqueUsernames.add(uname)
    


# Print statistics
print("\n##----------------------{STATS}----------------------##")
print("Runtime: " + str(round(time.time() - startTime,2)) + " seconds")
print("Restarts: " + str(restartCount))
print("Booty count: " + str(len(uniqueUsernames)) + " valid username(s)")
print("##----------------------{BOOTY}----------------------##")
# only print unique usernames
for uname in uniqueUsernames:
  print(uname)
print("##---------------------------------------------------##")

